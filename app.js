const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Grid = require('gridfs-stream');
const methodOverride = require('method-override');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const Vehicle = require('./models/vehicle')
const CarRequest = require('./models/carRequest')
const Model = require('./models/model')
const Make = require('./models/make')

const upload = require('./storage')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var requestsRouter = require('./routes/request')

mongoose.Promise = global.Promise

// env.load();

var app = express();

const crossOriginAllow = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
}

app.use(logger('dev'));
app.use(express.json());
app.use(bodyParser.json());
app.use(methodOverride('_method'));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(crossOriginAllow)

const mongoURI = process.env.MONGO_URI || 'mongodb://localhost:27017/car_listing_db';

const conn = mongoose.createConnection(mongoURI, { useNewUrlParser: true });

var gfs;

conn.once('open', () => {
    gfs = Grid(conn.db, mongoose.mongo);
    gfs.collection('uploads');
})

mongoose.connect(mongoURI).then(success => {
    console.log('successfully connected to database ')
}).catch(error => {
    console.log('connection error! ' + error)
})

app.post('/test', (req, res) => {
    res.send('Sending')
})

app.use('/users', usersRouter);

app.get('/vehicles', (req, res) => {
    let query = {};
    if (req.query) {
        if (req.query.phone) {
            query = {
                phone_number: req.query.phone
            }
        } else {
            query = {}
        }
    } else {
        query = {}
    }
    Vehicle.find(query).populate('images').populate([{path:"car_model", model:"Model", key:"id"}, {path:"car_make", model:"Make", key:"id"}]).exec()
        .then(response => {
            res.json(response)
        })
        .catch(error => res.json(error))
})

app.post('/vehicles', upload.array('files'), (req, res) => {
    if (req.files) {
        const images = req.files.map(file => file.id);
        let vehicle = new Vehicle({ ...req.body, images: images })
        vehicle.save()
            .then(result => {
                return res.json(result)
            })
            .catch(error => {
                return res.json(error)
            })
    } else {
        res.send({ success: false });
    }
})

app.get('/image/:filename', (req, res) => {
    gfs.files.findOne({ filename: req.params.filename }, (err, file) => {
        if (!file) {
            return res.status(404).json({ error: 'No File exists' })
        }
        if (file.contentType === 'image/png' || file.contentType === 'image/jpeg' || file.contentType === 'image/svg+xml') {
            const readStream = gfs.createReadStream(file.filename)
            readStream.pipe(res)
        } else {
            return res.status(404).json({ error: 'Not an Image' })
        }
    })
})

app.get('/vehicle/:id', (req, res) => {
    Vehicle.findOne({ _id: req.params.id }).exec().then(file => {
        return res.json(file)
    }).catch(error => {
        return res.json(error)
    })
})

app.post('/requests/car-request', (req, res) => {
    let request = new CarRequest(req.body)
    request.save()
        .then(response => {
            return res.json({ success: 1, response })
        })
        .catch(error => res.json(error))
})

app.get('/requests', (req, res) => {
    CarRequest.find().exec()
        .then(response => res.status(200).json(response)
            .catch(error => res.status(500).json(error)))
})

app.get('/models/:makeId', (req, res) => {
    let makeId = req.params.makeId;
    Model.find({makeid: makeId}).exec()
        .then(models => {
            res.status(200).json(models)
        })
        .catch(error => {
            res.status(500).json(error)
        })
})

app.get('/car_makes', (req, res) => {
    Make.find().exec()
        .then(models => {
            res.status(200).json(models)
        })
        .catch(error => {
            res.status(500).json(error)
        })
})

module.exports = app;
