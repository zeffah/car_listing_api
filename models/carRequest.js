const mongoose = require('mongoose')

const Schema = mongoose.Schema

const CarRequestSchema = new Schema({
    condition: { type: String },
    full_name: { type: String },
    cellphone: { type: String },
    email: { type: String },
    car_make: { type: String },
    car_model: { type: String },
    manufacturer_year: { type: String },
    engine_capacity: { type: String },
    color: { type: String },
}, { timestamps: true })

module.exports = mongoose.model('carRequest', CarRequestSchema)