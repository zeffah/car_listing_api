const mongoose = require('mongoose')

const Schema = mongoose.Schema

const VehicleMakeSchema = new Schema({
    id:{type:Number},
    make: {type: String}
})

module.exports = mongoose.model('Make', VehicleMakeSchema)