const mongoose = require('mongoose')

const Schema = mongoose.Schema

const VehicleModelSchema = new Schema({
    id: {type: Number},
    make: {type: String},
    model: {type: String},
    makeid: {type: String}
})

module.exports = mongoose.model('Model', VehicleModelSchema)