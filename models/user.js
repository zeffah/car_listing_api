const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    phone_number: {type: String},
    password: {type: String},
    isApproved: {type: Boolean, default: false}
}, {timestamps: true})

module.exports = mongoose.model('User', UserSchema)
