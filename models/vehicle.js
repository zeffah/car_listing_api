const mongoose = require('mongoose')

const Schema = mongoose.Schema
const VehicleSchema = new Schema({
    car_model: { type: String },
    car_type: { type: String },
    year: { type: Number },
    mileage: { type: Number },
    transmission: { type: String },
    condition: { type: String },
    car_make: { type: String },
    plate_number: { type: String },
    vin_number: { type: String },
    full_name: { type: String },
    price: { type: Number },
    phone_number: { type: String },
    county: {type: String},
    town: { type: String },
    images: [{ type: mongoose.SchemaTypes.ObjectId, ref: 'File'}]
})

VehicleSchema.virtual('model', {
  ref: 'Model',
  localField: 'car_model',
  foreignField: 'id',
  justOne: true,
});

VehicleSchema.virtual('make', {
  ref: 'Make',
  localField: 'id',
  foreignField: 'car_make',
  justOne: true,
});


module.exports = mongoose.model('Vehicle', VehicleSchema)