var express = require('express');
const fs = require('fs')
var router = express.Router();
const upload = require('../storage')
require('../app')
require('../models/images')
const VehicleImg = require('../models/vehicleImg')

const Vehicle = require('../models/vehicle')

router.route('/vehicles')
.get((req, res, next) => {
  Vehicle.find({}).populate('images').exec()
  .then(response => {
    // console.log(gfs)
    return res.json(response)
  })
  .catch(error =>res.json(error))

  // VehicleImg.find({}).then(vehicles=> {
  //   return res.json(vehicles)
  // })
  // .catch(error=>{
  //   return res.json(error)
  // })
})
.post(upload.array('files'), (req, res)=> {
  if (req.files) {
    // let vehicleImg = new VehicleImg();
    // for(let x = 0; x < req.files.length; x++){
    //   vehicleImg.img.data = fs.readFileSync(req.files[x].path)
    //   vehicleImg.img.contentType = req.files[x].contentType;
    // }
    // vehicleImg.save().then(result=>{
    //   return res.json(result)
    // })
    // .catch(error=>{
    //   return res.json(error)
    // });
    const images = req.files.map(file => file.id);
    let vehicle = new Vehicle({ ...req.body, images: images})
    vehicle.save()
    .then(result => {
      return res.json(result)
    })
    .catch(error => {
      return res.json(error)
    })
  }else{
    res.send({ success: false });
  }
})
router.route('/image/:filename')
.get((req, res) => {
    gfs.files.findOne({ filename: req.params.filename }, (err, file) => {
        if (!file) {
            return res.status(404).json({ error: 'No File exists' })
        }

        if (file.contentType === 'image/png' || file.contentType === 'image/jpeg') {
            const readStream = gfs.createReadStream(file.filename)
            readStream.pipe(res)
        } else {
            return res.status(404).json({ error: 'Not an Image' })
        }
    })
})

module.exports = router;
