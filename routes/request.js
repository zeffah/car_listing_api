var express = require('express');
var router = express.Router();
const CarRequest = require('../models/carRequest')

router.route('/car-request')
    .get((req, res)=>{
        CarRequest.find().exec()
        .then(response=>res.status(200).json(response)
        .catch(error=>res.status(500).json(error)))
    })
    .post((req, res)=>{
        let request = new CarRequest(req.body)
        request.save()
        .then(response => res.json(response))
        .catch(error => res.json(error))
    })