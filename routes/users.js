var express = require('express');
var router = express.Router();
const User = require('../models/user')

router.route('/admin/register')
  .post((req, res) => {
    let user = new User(req.body)
    user.save().then(response=>res.json(response)).catch(error=>res.json(error))
  })

router.route('/admin/login')
  .post((req, res) => {
    const {phone_number, password} = req.body
    console.log(req.body)
    User.findOne({phone_number: phone_number, password: password}, (error, user)=>{
      if(error){
        return res.json({success: 0, statusCode: 5000, message:error})
      }
      if(!user){
        return res.json({success: 0, statusCode: 404, message: 'User Not Found'})
      }
      if(!user.isApproved){
        return res.json({ success: 0, statusCode: 301, message: 'User not approved' })
      }
      return res.json({ success: 1, statusCode: 301, user:user })
    })
  })

module.exports = router;
