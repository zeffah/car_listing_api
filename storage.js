const GridFsStorage = require('multer-gridfs-storage');
const multer = require('multer');
const crypto = require('crypto')
const path  = require('path')

const mongoURI = process.env.MONGO_URI || 'mongodb://localhost:27017/car_listing_db';

const storage = new GridFsStorage({
    url: mongoURI, //mongodb://localhost:27017
    file: (req, file) => {
        return new Promise((resolve, reject) => {
            crypto.randomBytes(16, (err, buf) => {
                if(err){
                    return reject(err)
                }
                const filename = buf.toString('hex')+path.extname(file.originalname);
                const fileInfo = {
                    filename: filename,
                    metadata: req.body,
                    bucketName: 'uploads'
                };
                resolve(fileInfo)
            })
        })
    }
})

// const storage = multer.diskStorage({
//     destination: function (req, res, cb) {
//         cb(null, 'uploads/')
//     }
// })

const upload = multer({storage})

module.exports = upload